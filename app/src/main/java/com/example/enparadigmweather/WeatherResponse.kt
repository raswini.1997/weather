package com.example.enparadigmweather

import com.google.gson.annotations.SerializedName


class WeatherResponse {

    @SerializedName("timezone")
    var timezone:String = ""

    @SerializedName("current")
    var current:WeatherData? = null

    @SerializedName("daily")
    var daily:ArrayList<DailyWeatherData>? = null

}

class WeatherData {

    @SerializedName("temp")
    var temp:Double = 0.0

   @SerializedName("pressure")
    var pressure:Long = 0L

    @SerializedName("humidity")
    var humidity:Int = -1

    @SerializedName("wind_speed")
    var windSpeed:Double = 0.0

    @SerializedName("weather")
    var weather:ArrayList<Weather>? = null

}

class DailyWeatherData {

   @SerializedName("pressure")
    var pressure:Long = 0L

    @SerializedName("humidity")
    var humidity:Int = -1

    @SerializedName("wind_speed")
    var windSpeed:Double = 0.0

    @SerializedName("weather")
    var weather:ArrayList<Weather>? = null

}

class Weather {

    @SerializedName("main")
    var main:String = ""

    @SerializedName("description")
    var description:String = ""

}
