package com.example.enparadigmweather

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.enparadigmweather.KeyUtils.PLACE_PICKER_REQUEST
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.widget.Autocomplete
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var latLng: LatLng? = null
    var disposable:Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Places.initialize(baseContext, getString(R.string.api_key))

        tvAddress?.setOnClickListener {
            openPlacePicker(this)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                PLACE_PICKER_REQUEST -> {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    place.let {
                        tvAddress?.setText(place.address)
                        latLng = LatLng(place.latLng?.latitude!!, place.latLng?.longitude!!)

                        callApiForWeatherForecast()
                    }
                }
            }
        }
    }

    private fun callApiForWeatherForecast() {
        pbLoad.visibility = View.VISIBLE
        gpAllData.visibility = View.GONE
        val map = HashMap<String, Any>()
        map[ApiParam.WeatherMap.LAT] = latLng?.latitude?:0.0
        map[ApiParam.WeatherMap.LON] = latLng?.longitude?:0.0
        map[ApiParam.WeatherMap.EXCLUDE] = "hourly"
        map[ApiParam.WeatherMap.APPID] = getString(R.string.openweathermap_appid)

      disposable =   WebApiClient.webApi().callWeatherForecastApi2(map)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it?.let {
                    pbLoad.visibility = View.GONE
                    gpAllData.visibility = View.VISIBLE
                    handleWeatherData(it)
                }
            }, {
                pbLoad.visibility = View.GONE
                Toast.makeText(this,"Try Again...",Toast.LENGTH_SHORT).show()
            })
    }

    private fun handleWeatherData(data:WeatherResponse){
        var formatWeather = ""
        data.current?.let {
            formatWeather = "Current Weather:\nPressure:- ${it.pressure}\nHumidity: ${it.humidity}\nWindSpeed: ${it.windSpeed}\nWeather: ${it.weather?.get(0)?.description}"
            tvWeatherInfo.text = formatWeather
        }
        data.daily?.let {
            it.get(0).let {
                val comingDayWeatherFormat1 = "Tomorrow Weather Forecast:\nPressure:- ${it.pressure}\nHumidity: ${it.humidity}\nWindSpeed: ${it.windSpeed}\nWeather: ${it.weather?.get(0)?.description}"
                tvTmrwWeather.text = comingDayWeatherFormat1
            }

            it.get(1).let {
                val comingDayWeatherFormat2 = "The Day after tomorrow Weather Forecast:\nPressure:- ${it.pressure}\nHumidity: ${it.humidity}\nWindSpeed: ${it.windSpeed}\nWeather: ${it.weather?.get(0)?.description}"
                tvDayAfterTmrwWeather.text = comingDayWeatherFormat2
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

}