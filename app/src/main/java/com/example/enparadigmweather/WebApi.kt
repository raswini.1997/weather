package com.example.enparadigmweather

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface WebApi {
    @GET("data/2.5/onecall")
    fun callWeatherForecastApi2(
        @QueryMap param: HashMap<String, Any>
    ): Single<WeatherResponse>
}