package com.example.enparadigmweather

object ApiParam {

    object WeatherMap{
        const val LAT = "lat"
        const val LON = "lon"
        const val EXCLUDE = "exclude"
        const val APPID = "appid"
    }

}